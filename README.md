# Angular Test Employee

## Guide to run the project
1. After `git clone` use branch `main`. Install all package with `npm install` 
```
npm install
```
2. Create file `.env`, copy and paste this code below.
```
NEXT_PUBLIC_API_URL=http://localhost:3001
```
3. For fake API using `typicode/json-server`. Run `npx json-server employee.json`. The API will automatically ready to use 
```
json-server --watch db.json --port 3001
```
4. Run `npm run dev` on a **new terminal** for a dev server. Navigate to `http://localhost:3000/`. The application will automatically reload if you change any of the source files.
```
npm run dev
```
5. Login using 'john@example.com' & 'john@example.com' as credential. The username and password are same word `john@example.com`, or you can just register as new user.

## Further help
Please do contact me if there's any problem with the project. Thank you 
