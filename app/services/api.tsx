import axios from 'axios';
import { Profile, User } from '../interface/types';

const API_URL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:3001';

export const fetchUsers = async (): Promise<User[]> => {
  try {
    const response = await axios.get<User[]>(`${API_URL}/users`);
    return response.data;
  } catch (error) {
    console.error('Error fetching users:', error);
    throw error;
  }
};

export const login = async (email: string, password: string): Promise<boolean> => {
  try {
    const response = await axios.get<User[]>(`${API_URL}/users`);
    const users = response.data;
    const user = users.find((u) => u.email === email && u.password === password);
    if (user) {
      localStorage.setItem('id', user.id.toString());
      return true;
    } else {
      throw new Error('Invalid email or password');
    }
  } catch (error) {
    console.error('Error during login:', error);
    throw error;
  }
};

export const register = async (username: string, email: string, password: string, confirmPassword: string): Promise<boolean> => {
  if (password !== confirmPassword) {
    throw new Error('Passwords do not match');
  }

  try {
    const response = await axios.post<User>(`${API_URL}/users`, {
      username,
      email,
      password,
    });

    if (response.status === 201) { // Assuming 201 Created is the success status code
      localStorage.setItem('id', response.data.id.toString());
      return true;
    } else {
      throw new Error('Registration failed');
    }
  } catch (error) {
    console.error('Error during registration:', error);
    throw error;
  }
};


// Fetch a user by ID
export const fetchUserById = async (id: string): Promise<User> => {
  try {
    const response = await axios.get<User>(`${API_URL}/users/${id}`);
    return response.data;
  } catch (error) {
    console.error(`Error fetching user with ID ${id}:`, error);
    throw error;
  }
};

// Update user interests
export const updateInterests = async (id: string, interests: string[]): Promise<User> => {
  try {
    const response = await axios.patch<User>(`${API_URL}/users/${id}`, { interests });
    return response.data;
  } catch (error) {
    console.error(`Error updating interests for user with ID ${id}:`, error);
    throw error;
  }
};


export const updateImage = async (id: string, interests: string[]): Promise<User> => {
  try {
    const response = await axios.patch<User>(`${API_URL}/users/${id}`, { interests });
    return response.data;
  } catch (error) {
    console.error(`Error updating interests for user with ID ${id}:`, error);
    throw error;
  }
};


export const updateAbout = async (id: string, formData: Profile): Promise<User> => {
  try {
    const response = await axios.patch<User>(`${API_URL}/users/${id}`, formData);
    return response.data;
  } catch (error) {
    console.error(`Error updating user about info for user with ID ${id}:`, error);
    throw error;
  }
};