"use client"
import React, { useEffect, useState, ChangeEvent } from 'react';
import { useRouter } from 'next/navigation';
import About from '@/components/about';
import Interest from '@/components/interest';
import { fetchUserById } from '../services/api';
import { User } from '../interface/types';
import Spinner from '@/components/spinner';
import Horoscope from '@/components/horoscope';

const emptyUser: User = {
  id: '',
  username: '',
  displayName: '',
  email: '',
  password: '',
  birthday: '',
  profilePicture: '',
  height: '',
  weight: '',
  zodiac: '',
  horoscope: '',
  gender: '',
  interests: [],
};

export default function ProfilePage() {
  const [userData, setUserData] = useState<User>(emptyUser);
  const [loading, setLoading] = useState(true);
  const [backgroundImage, setBackgroundImage] = useState<string | null>(null);
  const router = useRouter();

  const fetchProfile = async () => {
    try {
      const id = localStorage.getItem('id');
      if (!id) {
        router.push('/auth/login');
        return;
      }

      const user = await fetchUserById(id);
      if (!user.id) {
        router.push('/auth/login');
        return;
      }
      setUserData(user);
      setLoading(false);
    } catch (error) {
      console.error('Error fetching profile:', error);
    }
  };

  const handleImageUpload = (e: ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onloadend = () => {
        setBackgroundImage(reader.result as string);
      };
      reader.readAsDataURL(file);
    }
  };


  useEffect(() => {
    updateUserData();
  }, [userData]);

  const updateUserData = async () => {
    await fetchProfile();
  };

  if (loading) {
    return (
      <div className="min-h-screen flex items-center justify-center">
        <Spinner />
      </div>
    );
  }

  return (userData && (
    <section className="min-h-screen flex flex-col items-center justify-start bg-app px-2 gap-4">
      <div className='p-2'>@{userData['username']}</div>

      <div className="card h-44 w-full rounded-3xl p-4 bg-profile" style={{ position: 'relative', overflow: 'hidden', }} >
        <div className='flex justify-end relative z-20'>
          <label htmlFor="imageUpload">
            <button onClick={() => {const input = document.getElementById('imageUpload');if (input) {input.click();}}}>
              <svg
                width="17"
                height="17"
                viewBox="0 0 17 17"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M9.39248 2.54999L3.57706 8.70541C3.35748 8.93916 3.14498 9.39957 3.10248 9.71832L2.84039 12.0133C2.74831 12.8421 3.34331 13.4087 4.16498 13.2671L6.44581 12.8775C6.76456 12.8208 7.21081 12.5871 7.43039 12.3462L13.2458 6.19082C14.2516 5.12832 14.705 3.91707 13.1396 2.43666C11.5812 0.970408 10.3983 1.48749 9.39248 2.54999Z"
                  stroke="white"
                  strokeWidth="1.0625"
                  strokeMiterlimit="10"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M8.42209 3.57703C8.72667 5.53203 10.3133 7.02661 12.2825 7.22494"
                  stroke="white"
                  strokeWidth="1.0625"
                  strokeMiterlimit="10"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
                <path
                  d="M2.125 15.5834H14.875"
                  stroke="white"
                  strokeWidth="1.0625"
                  strokeMiterlimit="10"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                />
              </svg>
            </button>
          </label>
          <input
            type="file"
            id="imageUpload"
            accept="image/*"
            style={{ display: 'none' }}
            onChange={handleImageUpload}
          />
        </div>
        <div className='flex justify-end flex-col items-start h-full pb-4 relative z-20'>
          @{userData['username']}
          <small className='capitalize'>{userData['gender']}</small>
          <div className="flex gap-4">
            {userData['horoscope'] && (
              <Horoscope horoscope={userData['horoscope']} />
            )}
            {userData['zodiac'] && (
              <small className='bg-gray-900 rounded-full py-2 px-4 flex gap-2'>
                {userData['zodiac']}
              </small>
            )}

          </div>
        </div>
        <div className="bg-profile absolute inset-0 z-10"></div>

        {backgroundImage && (
          <div
            className="absolute inset-0 -z-0"
            style={{
              backgroundImage: `url(${backgroundImage})`,
              backgroundSize: 'cover',
            }}
          ></div>
        )}
      </div>

      <About updateUserData={updateUserData} />

      <Interest tags={userData['interests']} updateUserData={updateUserData} />

    </section>
  )
  );
}
