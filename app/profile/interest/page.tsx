"use client"
import React, { useEffect, useState, ChangeEvent } from 'react';
import { useRouter } from 'next/navigation';
import About from '@/components/about';
import Interest from '@/components/interest';
import TagInput from '@/components/tagInput';
import { fetchUserById, updateInterests } from '@/app/services/api';

interface Profile {
    name: string;
    email: string;
}

const InterestPage: React.FC = () => {
    const [interests, setInterests] = useState<string[]>([]);
    const router = useRouter();

    const handleTagsChange = (newTags: string[]) => {
        setInterests(newTags);
    };

    const handleSave = async () => {
        if (!interests) {
            router.push('/profile');
            return;
        }
        try {
            const userId = localStorage.getItem('id');
            if (userId) {
                await updateInterests(userId, interests);
                router.push('/profile');
            }
        } catch (error) {
            console.error('Error updating user about info:', error);
        }
    }
    const handleBack = async () => {
        router.push('/profile');
    }

    return (
        <section className="min-h-screen flex flex-col items-center justify-start bg-gradient px-2 gap-4">
            <nav className='p-2 flex items-center justify-between w-full'> <button onClick={handleBack}> Back</button> <button className='btn text-blue-gradient font-bold' onClick={handleSave}>Save</button></nav>
            <div className='w-full flex gap-6 flex-col px-4 py-10'>
                <div className="flex gap-2 flex-col">
                    <p className='text-golden text-left px-2 font-bold'>Tell everyone about yourself</p>
                    <h2 className='text-white text-2xl px-2 text-left font-bold w-full'>What interest you ?</h2>
                </div>
                <TagInput onTagsChange={handleTagsChange} />
            </div>

        </section>
    );
};

export default InterestPage;
