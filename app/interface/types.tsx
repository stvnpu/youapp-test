export interface User {
  id: string;
  username: string;
  displayName: string;
  email: string;
  password: string;
  birthday: string;
  profilePicture: string;
  height: string;
  weight: string;
  zodiac: string;
  horoscope: string;
  gender: string;
  interests: string[];
}


export interface Profile {
  profilePicture: string;
  displayName: string;
  gender: string;
  birthday: string;
  horoscope: string;
  zodiac: string;
  height: string;
  weight: string;
}