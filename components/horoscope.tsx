// components/Horoscope.tsx

import React from 'react';

interface HoroscopeProps {
  horoscope: string;
}

const Horoscope: React.FC<HoroscopeProps> = ({ horoscope }) => {
  if (!horoscope) return null;

  const svgPath = `/horoscopes/${horoscope}.svg`;

  return (
    <small className='bg-gray-900 rounded-full py-2 px-4 flex gap-2 items-center'>
      <img src={svgPath} alt={horoscope} width="15" height="15" className='h-4 w-4' />
      {horoscope}
    </small>
  );
};

export default Horoscope;
