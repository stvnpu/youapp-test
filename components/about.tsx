import React, { useState, ChangeEvent, FocusEvent, useEffect } from 'react';
import { clsx } from 'clsx';
import { Profile } from '@/app/interface/types';
import { fetchUserById, updateAbout } from '@/app/services/api';

interface AboutProps {
    updateUserData: () => void;
}

type FormData = {
    displayName: string;
    gender: string;
    birthday: string;
    horoscope: string;
    zodiac: string;
    height: string;
    weight: string;
};

const calculateAge = (birthday: string): number => {
    const today = new Date();
    const birthDate = new Date(birthday);
    let age = today.getFullYear() - birthDate.getFullYear();
    const monthDifference = today.getMonth() - birthDate.getMonth();
    if (monthDifference < 0 || (monthDifference === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
};

const formatDate = (date: string): string => {
    try {
        const [year, month, day] = date.split('-');
        return `${day}/${month}/${year}`;
    } catch (error) {
        return date;
    }
};


const getHoroscope = (date: string): string => {
    const horoscopes = [
        { sign: 'Capricorn', start: '01-01', end: '01-19' },
        { sign: 'Aquarius', start: '01-20', end: '02-18' },
        { sign: 'Pisces', start: '02-19', end: '03-20' },
        { sign: 'Aries', start: '03-21', end: '04-19' },
        { sign: 'Taurus', start: '04-20', end: '05-20' },
        { sign: 'Gemini', start: '05-21', end: '06-20' },
        { sign: 'Cancer', start: '06-21', end: '07-22' },
        { sign: 'Leo', start: '07-23', end: '08-22' },
        { sign: 'Virgo', start: '08-23', end: '09-22' },
        { sign: 'Libra', start: '09-23', end: '10-22' },
        { sign: 'Scorpio', start: '10-23', end: '11-21' },
        { sign: 'Sagittarius', start: '11-22', end: '12-21' },
        { sign: 'Capricorn', start: '12-22', end: '12-31' }
    ];
    const [month, day] = date.split('-').slice(1).map(Number);
    const formattedDate = new Date(0, month - 1, day);

    return horoscopes.find(({ start, end }) => {
        const [startMonth, startDay] = start.split('-').map(Number);
        const [endMonth, endDay] = end.split('-').map(Number);
        const startDate = new Date(0, startMonth - 1, startDay);
        const endDate = new Date(0, endMonth - 1, endDay);
        return formattedDate >= startDate && formattedDate <= endDate;
    })?.sign || '';
};

const getZodiac = (year: number): string => {
    const zodiacs = [
        'Rat', 'Ox', 'Tiger', 'Rabbit', 'Dragon', 'Snake',
        'Horse', 'Goat', 'Monkey', 'Rooster', 'Dog', 'Pig'
    ];
    return zodiacs[(year - 4) % 12];
};

const About: React.FC<AboutProps> = ({ updateUserData }) => {
    const [mode, setMode] = useState<'base' | 'edit' | 'view'>('base');
    const [formData, setFormData] = useState<Profile>({
        profilePicture: '',
        displayName: '',
        gender: '',
        birthday: '',
        horoscope: '',
        zodiac: '',
        height: '',
        weight: '',
    });

    const [activeInput, setActiveInput] = useState<{ height: boolean; weight: boolean }>({
        height: false,
        weight: false,
    });

    useEffect(() => {
        const userId = localStorage.getItem('id');
        if (userId) {
            fetchUserById(userId).then(data => {
                console.log(data);

                setFormData({
                    profilePicture: data.profilePicture || '',
                    displayName: data.displayName || '',
                    gender: data.gender || '',
                    birthday: data.birthday || '',
                    horoscope: data.horoscope || '',
                    zodiac: data.zodiac || '',
                    height: data.height || '',
                    weight: data.weight || '',
                });

                if (!data.birthday && !data.displayName && !data.height && !data.weight) {
                    setMode('base');
                } else {
                    setMode('view');
                }
            });
        }
    }, []);


    const handleInputChange = (e: ChangeEvent<HTMLInputElement | HTMLSelectElement>) => {
        const { name, value } = e.target;
        const updatedData = { ...formData, [name]: value };

        if (name === 'birthday') {
            updatedData.horoscope = getHoroscope(value);
            updatedData.zodiac = getZodiac(new Date(value).getFullYear());
        }

        setFormData(updatedData);
    };

    const handleFocus = (e: FocusEvent<HTMLInputElement>) => {
        setActiveInput({
            ...activeInput,
            [e.target.name]: true,
        });
    };

    const handleBlur = (e: FocusEvent<HTMLInputElement>) => {
        setActiveInput({
            ...activeInput,
            [e.target.name]: false,
        });
    };

    const handleModeChange = (newMode: 'base' | 'edit' | 'view') => {
        setMode(newMode);
    };

    const handleSave = async () => {
        try {
            const userId = localStorage.getItem('id');
            if (userId) {
                await updateAbout(userId, formData);
                handleModeChange('view');
            }
        } catch (error) {
            console.error('Error updating user about info:', error);
        }
    };

    const renderBaseMode = () => (
        <div className='card-secondary min-h-32 w-full rounded-3xl py-4 px-6'>
            <div className='flex justify-between'>
                <p className='font-semibold'>About</p>
                <button onClick={() => handleModeChange('edit')}>
                    <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.39248 2.54999L3.57706 8.70541C3.35748 8.93916 3.14498 9.39957 3.10248 9.71832L2.84039 12.0133C2.74831 12.8421 3.34331 13.4087 4.16498 13.2671L6.44581 12.8775C6.76456 12.8208 7.21081 12.5871 7.43039 12.3462L13.2458 6.19082C14.2516 5.12832 14.705 3.91707 13.1396 2.43666C11.5812 0.970408 10.3983 1.48749 9.39248 2.54999Z" stroke="white" strokeWidth="1.0625" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M8.42209 3.57703C8.72667 5.53203 10.3133 7.02661 12.2825 7.22494" stroke="white" strokeWidth="1.0625" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M2.125 15.5834H14.875" stroke="white" strokeWidth="1.0625" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                </button>
            </div>
            <div className='flex justify-start items-end h-full opacity-50 text-md tracking-tight font-light mt-5'>
                Add in your details to help others know you better
            </div>
        </div>
    );

    const renderEditMode = () => (
        <div className='card-secondary min-h-32 w-full rounded-3xl py-4 px-6'>
            <div className='flex justify-between'>
                <p className='font-semibold'>About</p>
                <button onClick={handleSave} className='text-golden tracking-tight text-sm font-semibold'>Save & Update</button>
            </div>
            <div className='flex flex-col space-y-4 mt-4'>
                {/* IMAGE COMPONENT NEEDED */}
                <div className='flex justify-between gap-4 items-center'>
                    <label className='w-auto text-nowrap text-sm opacity-20'> Display Name:</label>
                    <input
                        className='w-52 px-5 text-sm text-right !h-9 rounded-lg !min-h-0 mb-0'
                        type='text'
                        name='displayName'
                        value={formData.displayName}
                        onChange={handleInputChange}
                        placeholder='Enter name'
                    />
                </div>
                <div className='flex justify-between gap-4 items-center'>
                    <label className='w-auto text-nowrap text-sm opacity-20'> Gender:</label>
                    <select
                        className='w-52 px-5 text-sm text-right !h-9 rounded-lg !min-h-0 mb-0'
                        name='gender'
                        value={formData.gender}
                        onChange={handleInputChange}
                    >
                        <option value=''>Select Gender</option>
                        <option value='male'>Male</option>
                        <option value='female'>Female</option>
                    </select>
                </div>

                <div className='flex justify-between gap-4 items-center'>
                    <label className='w-auto text-nowrap text-sm opacity-20'> Birthday:</label>
                    <input
                        className='w-52 px-5 text-sm text-right !h-9 rounded-lg !min-h-0 mb-0'
                        type='date'
                        name='birthday'
                        value={formData.birthday}
                        onChange={handleInputChange}
                        placeholder='DD MM YYYY'
                    />
                </div>

                <div className='flex justify-between gap-4 items-center'>
                    <label className='w-auto text-nowrap text-sm opacity-20'> Horoscope:</label>
                    <input
                        className='w-52 px-5 text-sm text-right !h-9 rounded-lg !min-h-0 mb-0'
                        type='text'
                        name='horoscope'
                        value={formData.horoscope}
                        onChange={handleInputChange}
                        placeholder='--'
                        readOnly
                    />
                </div>
                <div className='flex justify-between gap-4 items-center'>
                    <label className='w-auto text-nowrap text-sm opacity-20'> Zodiac:</label>
                    <input
                        className='w-52 px-5 text-sm text-right !h-9 rounded-lg !min-h-0 mb-0'
                        type='text'
                        name='zodiac'
                        value={formData.zodiac}
                        onChange={handleInputChange}
                        placeholder='--'
                        readOnly
                    />
                </div>

                <div className='flex justify-between gap-4 items-center'>
                    <label className='w-auto text-nowrap text-sm opacity-20'> Height:</label>
                    <div className='relative w-52'>
                        <input
                            className={clsx(
                                'w-full text-sm text-right !h-9 rounded-lg !min-h-0 mb-0',
                                formData.height || activeInput.height ? 'px-12' : 'px-5'
                            )}
                            type='text'
                            name='height'
                            value={formData.height}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleInputChange}
                            placeholder='Add Height'
                        />
                        {(formData.height || activeInput.height) && (
                            <span className='absolute right-5 top-1/2 transform -translate-y-1/2 text-sm'>cm</span>
                        )}
                    </div>
                </div>

                <div className='flex justify-between gap-4 items-center'>
                    <label className='w-auto text-nowrap text-sm opacity-20'> Weight:</label>
                    <div className='relative w-52'>
                        <input
                            className={clsx(
                                'w-full text-sm text-right !h-9 rounded-lg !min-h-0 mb-0',
                                formData.weight || activeInput.weight ? 'px-12' : 'px-5'
                            )}
                            type='text'
                            name='weight'
                            value={formData.weight}
                            onFocus={handleFocus}
                            onBlur={handleBlur}
                            onChange={handleInputChange}
                            placeholder='Add Weight'
                        />
                        {(formData.weight || activeInput.weight) && (
                            <span className='absolute right-6 top-1/2 transform -translate-y-1/2 text-sm'>kg</span>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );

    const renderViewMode = () => (
        <div className='card-secondary min-h-32 w-full rounded-3xl py-4 px-6'>
            <div className='flex justify-between'>
                <p className='font-semibold'>About</p>
                <button onClick={() => handleModeChange('edit')}>
                    <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M9.39248 2.54999L3.57706 8.70541C3.35748 8.93916 3.14498 9.39957 3.10248 9.71832L2.84039 12.0133C2.74831 12.8421 3.34331 13.4087 4.16498 13.2671L6.44581 12.8775C6.76456 12.8208 7.21081 12.5871 7.43039 12.3462L13.2458 6.19082C14.2516 5.12832 14.705 3.91707 13.1396 2.43666C11.5812 0.970408 10.3983 1.48749 9.39248 2.54999Z" stroke="white" strokeWidth="1.0625" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M8.42209 3.57703C8.72667 5.53203 10.3133 7.02661 12.2825 7.22494" stroke="white" strokeWidth="1.0625" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M2.125 15.5834H14.875" stroke="white" strokeWidth="1.0625" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                </button>
            </div>
            <div className='flex flex-col space-y-2 mt-4'>
                <p><span className='opacity-30 text-sm'>Birthday:</span>{formData.birthday ? (<>{formatDate(formData.birthday)} (Age {calculateAge(formData.birthday)})</>) : ("-")}</p>
                <p><span className='opacity-30 text-sm'>Horoscope:</span> {formData.horoscope ? formData.horoscope : '-'}</p>
                <p><span className='opacity-30 text-sm'>Zodiac:</span> {formData.zodiac ? formData.zodiac : '-'}</p>
                <p><span className='opacity-30 text-sm'>Height:</span> {formData.height ? <>{formData.height} cm</> : '-'} </p>
                <p><span className='opacity-30 text-sm'>Weight:</span> {formData.weight ? <>{formData.weight} kg</> : '-'}  </p>
            </div>
        </div>
    );

    return (
        <>
            {mode === 'base' && renderBaseMode()}
            {mode === 'edit' && renderEditMode()}
            {mode === 'view' && renderViewMode()}
        </>
    );
};

export default About;