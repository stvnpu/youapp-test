import { fetchUserById } from '@/app/services/api';
import React, { useState, useRef, useEffect } from 'react';

interface TagInputProps {
  onTagsChange: (tags: string[]) => void; // Callback function prop
}

const TagInput: React.FC<TagInputProps> = ({ onTagsChange }) => {
  const [tags, setTags] = useState<string[]>([]);
  const [inputValue, setInputValue] = useState<string>('');
  const inputRef = useRef<HTMLDivElement>(null);
  const placeholderText = "Type and press enter";

  useEffect(() => {
    const userId = localStorage.getItem('id');
    if (userId) {
      fetchUserById(userId).then(data => {
        console.log(data);
        if (data && data.interests && data.interests.length > 0) {
          setTags(data.interests);
        }
      });
    }
  }, []);

  const handleKeyDown = (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.key === 'Enter' && inputValue.trim()) {
      e.preventDefault();
      setTags([...tags, inputValue.trim()]);
      setInputValue('');
      if (inputRef.current) {
        inputRef.current.innerText = '';
      }
      onTagsChange([...tags, inputValue.trim()]);
    }
  };

  const handleInput = () => {
    if (inputRef.current) {
      setInputValue(inputRef.current.innerText);
    }
  };

  const handleRemoveTag = (tagToRemove: string) => {
    setTags(tags.filter(tag => tag !== tagToRemove));
    onTagsChange(tags.filter(tag => tag !== tagToRemove));
  };

  useEffect(() => {
    const div = inputRef.current;
    if (div) {
      if (div.textContent === '') {
        div.innerHTML = `<span class="placeholder opacity-40">${placeholderText}</span>`;
      }

      const handleFocus = () => {
        if (div.textContent === placeholderText) {
          div.innerHTML = '';
        }
      };

      const handleBlur = () => {
        if (inputRef.current) {
          if (inputRef.current.textContent?.trim() === '') {
            setInputValue('');
            inputRef.current.innerHTML = `<span class="placeholder opacity-40">${placeholderText}</span>`;
          }
        }
      };

      div.addEventListener('focus', handleFocus);
      div.addEventListener('blur', handleBlur);

      return () => {
        div.removeEventListener('focus', handleFocus);
        div.removeEventListener('blur', handleBlur);
      };
    }
  }, []);

  return (
    <>
      <div className="flex interest-container flex-wrap gap-2 items-center p-2 rounded-lg px-4">
        {tags.length > 0 && (
          <>
            {tags.map((tag, index) => (
              <div
                key={index}
                className="bg-tag text-sm text-white font-semibold rounded px-2 py-1 flex items-center space-x-2"
              >
                <span>{tag}</span>
                <span className="cursor-pointer" onClick={() => handleRemoveTag(tag)}>
                  &times;
                </span>
              </div>
            ))}
          </>
        )}
        <div
          ref={inputRef}
          contentEditable
          onInput={handleInput}
          onKeyDown={handleKeyDown}
          className="outline-none flex-grow bg-transparent"
          style={{ minWidth: '50px' }}
          suppressContentEditableWarning={true}
        />
      </div>
    </>
  );
};

export default TagInput;
