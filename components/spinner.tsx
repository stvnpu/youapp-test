import React from 'react';

const Spinner = () => {
  return (
    <div className="flex items-center justify-center space-x-2">
      <div className="w-4 h-4 border-4 border-t-transparent border-white border-solid rounded-full animate-spin"></div>
      <div>Loading...</div>
    </div>
  );
};

export default Spinner;